#!/usr/bin/env python3

# copyright Matt Davidson 2017
# example from https://www.airpair.com/python/posts/django-flask-pyramid

from security import security
from common import app, db, port, manager, config, conf_attr
from flask import g
from metrics import send_metric
from logging import getLogger
from pprint import pprint

log = getLogger('flask')

@app.before_request
def load_config(file='config.yml',attr='conf'):
    setattr(g, conf_attr, config)


@manager.command
def run(host='0.0.0.0', port=port, **kwargs):
    send_metric('start', 1)
    app.run(host=host, port=port, **kwargs)


if __name__ == '__main__':
    import models
    import routes
    import api
    db.create_all()
    #log.debug(pprint(app.config))
    manager.run()

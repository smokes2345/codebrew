from socket import create_connection
from os import environ
from datetime import datetime
from common import log


key = environ.get('HOSTEDGRAPHITE_APIKEY')
def send_metric(name, value, timestamp=True):
    if key:
        graphite = create_connection(("dd7fa179.carbon.hostedgraphite.com", 2003))
        metric = '.'.join((key,name))
        if timestamp:
            vals = (str(metric), str(value), str(datetime.utcnow()))
        else:
            vals = (str(metric), str(value))
        graphite.send(' '.join(vals).encode('utf-8'))
        graphite.close()
    else:
        log.warn("Unable to detect graphite key")
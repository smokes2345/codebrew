from common import db
from utils import TempUnitEnum
from utils.db.types import Temperature


class Item(db.Model):
    __tablename__ = 'items'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    name = db.Column(db.String(63))
    brand = db.Column(db.String(63), nullable=True)
    measure = db.Column(db.Float)
    unit = db.Column(db.Enum('oz', 'g', 'lb', 'gal', 'cup', 'fl oz', name='unit_enum'))
    price = db.Column(db.Float(precision=2), nullable=True)

    def __str__(self):
        return "{} {} {}".format(self.name, self.size, self.size_unit)


class Grain(Item):
    __tablename__ = 'grains'
    id = db.Column(db.Integer, db.ForeignKey('items.id'), primary_key=True)
    type = db.Column(db.String(127))


class Sugar(Item):
    __tablename__ = 'sugars'
    id = db.Column(db.Integer, db.ForeignKey('items.id'), primary_key=True)
    source = db.Column(db.String(63), nullable=True)


class Yeast(Item):
    __tablename__ = 'yeasts'
    id = db.Column(db.Integer, db.ForeignKey('items.id'), primary_key=True)
    ounces = db.Column(db.Integer, nullable=False)
    genus = db.Column(db.String(63), nullable=True)
    species = db.Column(db.String(63), nullable=True)
    store_link = db.Column(db.String(127), nullable=True)


class IngredientList(db.Model):
    __tablename__ = 'ingredient_lists'
    id = db.Column(db.Integer, primary_key=True)
    brew_id = db.Column(db.Integer, db.ForeignKey('brew.id'))
    component = db.Column(db.Integer, db.ForeignKey('items.id'))
    name = db.Column(db.Text)


class Step(db.Model):
    __tablename__ = 'recipe'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(127), nullable=True)
    brew = db.Column(db.Integer, db.ForeignKey('brew.id'))
    yeast = db.Column(db.Integer, db.ForeignKey('yeasts.id'))
    base = db.Column(db.String(127))
    sugar = db.Column(db.Integer, db.ForeignKey('sugars.id'))
    ingredients = db.Column(db.Integer, db.ForeignKey('ingredient_lists.id'))


class Hydrometer(db.Model):
    __tablename__ = 'hydrometer'
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey('user.id'))
    calibrated_temp = db.Column(Temperature)
    #temp_unit = db.Column(db.Enum(TempUnitEnum, name='hydro_temp_unit_enum'))


class GravityReading(db.Model):
    __tablename__ = 'gravity'
    id = db.Column(db.Integer, primary_key=True)
    brew = db.Column(db.Integer, db.ForeignKey('brew.id'))
    gravity = db.Column(db.Float(precision=2), nullable=False)
    temp = db.Column(Temperature)
#    temp_unit = db.Column(db.Enum('F','C', name='grav_temp_unit_enum'))
    notes = db.Column(db.Text)


class Brew(db.Model):
    __tablename__ = 'brew'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(127), nullable=False)
    style = db.Column(db.String(63), nullable=True)
    abv = db.Column(db.Float(precision=1), nullable=True)
    instrcutions = db.Column(db.Text, nullable=False)
    owner = db.Column(db.Integer, db.ForeignKey('user.id'))
    #recipe = db.Column(db.Integer, db.ForeignKey('recipe.id'))

class Batch(db.Model):
    __tablename__ = 'batch'
    id = db.Column(db.Integer, primary_key=True)
    brew = db.Column(db.Integer, db.ForeignKey('brew.id'))
    mash_temp = db.Column(Temperature)
    og = db.Column(db.Float, nullable=True)
    fg = db.Column(db.Float, nullable=True)

class Comment(db.Model):
    __tablename__ = 'comment'
    id = db.Column(db.Integer, primary_key=True)
    brew = db.Column(db.Integer, db.ForeignKey('brew.id'), nullable=True)
    recipe = db.Column(db.Integer, db.ForeignKey('recipe.id'), nullable=True)
    content = db.Column(db.Text)

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from os import environ, path
from logging import INFO
from yaml import load

# setup app
port = int(environ.get('PORT', 5000))
app = Flask(__name__)
app.logger.setLevel(INFO)
log = app.logger
# We'll just use SQLite here so we don't need an external database
if 'DATABASE_URL' in environ:
    app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DATABASE_URL')
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root@localhost/brewlog'

# config goes here
app.config['SQLALCHEMY_ECHO'] = True

app.config['SECRET_KEY'] = 'this is the super secret session token, but don\'t tell anyone else'

app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_CONFIRMABLE'] = True
app.config['SECURITY_RECOVERABLE'] = True

db = SQLAlchemy(app=app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.help_args = (['--help'])
manager.add_command('db', MigrateCommand)

# setup mail
from flask_mail import Mail,  Message
app.config.update(
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_SSL=True,
    MAIL_USERNAME = 'smokes2345@gmail.com',
    MAIL_PASSWORD = 'tzulxnhjjxedqbvx'
)
mail = Mail(app)

conf_file = 'config.yml'
conf_attr = 'conf'
config = {}
if path.isfile(conf_file):
    with open(conf_file, 'r') as fh:
        config = load(fh)
"""empty message

Revision ID: ef676c91c7a3
Revises: d739450492d5
Create Date: 2017-06-28 17:40:08.982057

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ef676c91c7a3'
down_revision = 'd739450492d5'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass

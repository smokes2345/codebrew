from common import app, db
from forms import form_factory
from flask import g, request, redirect, url_for
from flask_security import current_user
import models


def verify_model(model):
    if hasattr(models, model):
        return True
    return False


def return_to_sender(request=request):
    def invalidate_caches(response):
        response.headers['Cache-Control'] = 'public, max-age=0'

        return response
    app.after_request(invalidate_caches)
    return redirect(request.referrer)


def get_model(model,id=None):
    model_name = model
    if id:
        model = getattr(models, model_name).query.get(id)
    else:
        model = getattr(models, model_name)()
    if hasattr(model, 'user'):
        model.user = current_user.id
    return model


def has_id():
    if 'id' in get_body():
        return True
    return False


def get_body():
    if request.is_json:
        return request.get_json()
    else:
        return request.args

@app.route('/api/', methods=['POST'])
def process_request():
    pass


@app.route('/api/<string:model>', methods=['POST'])
def create(model):
    pass


@app.route('/api/<string:model>/<int:id>', methods=['PUT'])
def update(model, id):
    pass


@app.route('/api/<string:model>/<int:id>', methods=['GET'])
def read(model, id=None):
    pass


@app.route('/api/<string:model>/<int:id>', methods=['DELETE'])
def delete(model, id):
    pass


@app.route('/api/<string:model>', methods=['PUT', 'POST'])
def create_or_update(model):
    body = get_body()
    model = get_model(model or body.get('model'), id)
    if has_id():
        return update(model, id)
    else:
        return create(model)
#    if not request.is_json:
#        form = form_factory(type(model))(request.form)
#    else:
#        form = form_factory(type(model))(request.get_json())
#    form.populate_obj(model)
#    db.session.add(model)
#    db.session.commit()
#    return return_to_sender()

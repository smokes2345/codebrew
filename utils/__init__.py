from enum import Enum

per_user_items = []
def per_user(model, count=1):
    if hasattr(model, 'user') and model not in per_user_items:
        per_user_items.append(model)


class TempUnitEnum(Enum):
    F = 0
    C = 1


def convert_f_to_c(temp):
    return (temp - 32)  / 9.0 * 5.0


def convert_c_to_f(temp):
    return 9.0 / 5.0 * temp + 32


def convert_temp(temp, unit):
    unit = unit.lower()
    if unit == "c":
        temp = 9.0 / 5.0 * temp + 32
        return temp
    if unit == "f":
        temp = (temp - 32)  / 9.0 * 5.0
        return temp
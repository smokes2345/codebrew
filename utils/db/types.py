import sqlalchemy.types as types
from flask_security import current_user
from utils import convert_temp, TempUnitEnum


class Temperature(types.TypeDecorator):

    impl = types.Float

    def process_bind_param(self, value, dialect):
        if current_user.temp_unit == TempUnitEnum.C:
            return convert_temp(value, current_user.temp_unit)
        else: return value

    def process_result_value(self, value, dialect):
        if current_user.temp_unit == TempUnitEnum.C:
            return convert_temp(value, TempUnitEnum.F)
        else: return value

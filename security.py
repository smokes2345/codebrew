import enum
from common import db, app
from pytz import common_timezones
from sqlalchemy import Enum
from flask_security import SQLAlchemyUserDatastore, Security, UserMixin, RoleMixin
from utils import TempUnitEnum

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
#    username = db.Column(db.String(63), unique=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    locale = db.Column(db.String(255))
    tz = db.Column(db.Enum(*common_timezones, name='tz_enum'))
    temp_unit = db.Column(db.Enum(TempUnitEnum, name='user_temp_unit_enum'))
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return '<User id=%s email=%s>' % (self.id, self.email)

# Setup security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

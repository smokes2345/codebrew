from flask import render_template, g, request, url_for
from common import db, app
from flask_security import login_required, current_user
from forms import form_factory
from security import User
import models

settings_objs = ['Hydrometer']

@login_required
@app.route('/settings', methods=['GET'])
def render_settings():
    user_id = current_user.id
    user = User.query.get(user_id)
    user_form = form_factory(User)(obj=user)
    form_list = [
        {
            'name': User.__name__,
            'content': user_form,
            'action': url_for('create_or_update', model=User.__name__),
            'submit': 'Save'
        }
    ]
    for obj in settings_objs:
        if hasattr(models, obj):
            model = getattr(models, obj)
            user_obj = model.query.filter_by(user=user_id).first()
            model_form = form_factory(model, include_keys=False)(obj=user_obj)
            form_list.append({
                'name': model.__name__,
                'content': model_form,
                'action': url_for('create_or_update', model=model.__name__),
                'submit': 'Save'
            })
    return render_template('settings.html.j2', forms=form_list)


@login_required
@app.route('/settings', methods=['POST'])
def save_settings():
    pass
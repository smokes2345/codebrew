from security import User, Role
from models import Hydrometer
from flask_security import login_required
from common import app, db


@login_required
@app.route("/settings/<int:user>", methods=['GET'])
def settings(user=None):
    forms = []



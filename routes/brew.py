from common import app, db
from models import Brew, GravityReading
from forms import form_factory
from flask import render_template, redirect, url_for, request, g, flash
from flask_security import login_required, current_user


@app.route("/", methods=['GET'])
def root():
    return redirect('/brews')
#    brews = Brew.query.all()
#    form = BrewForm()
#    return render_template('index.html', form=BrewForm(), brews=brews)


@app.route('/brews', methods=['GET'])
@login_required
def list_brew():
    form = form_factory(Brew)()
    brews = Brew.query.filter_by(owner=current_user.id)
    return render_template('brew_list.html.j2', form=form, brews=brews)


@app.route('/brew', methods=['POST'])
@login_required
def add_brew():
    form = form_factory(Brew)(request.form)
    if form.id.data:
        brew = Brew.query.get(form.id.data)
        id = brew.id
    else:
        brew = Brew()
        id = None
    if form.validate():
        brew.owner = current_user.id
        form.populate_obj(brew)
        db.session.add(brew)
        db.session.commit()
#        return redirect(url_for('brew', id=brew.id))
    else:
        flash("Please complete all fields")
 #       return redirect(url_for('brew'))
    return redirect(url_for('brew', id=id))


@login_required
@app.route('/brew', methods=['GET'])
def brew(id=None):
    id = id or request.args.get('id')
    if id:
        brew = Brew.query.get(id)
        gravity = GravityReading.query.filter_by(brew=brew.id)
        return render_template(
            'brew.html.j2',
            brew_form=form_factory(Brew)(obj=brew),
            gravity_form=form_factory(GravityReading)(obj=gravity)
        )
    else:
        brew = Brew()
        gravity = GravityReading()
        return render_template(
            'brew.html.j2',
            brew_form=form_factory(Brew)(obj=brew)
        )


@login_required
@app.route('/brew', methods=['DELETE'])
def delete_brew():
    brewId = request.args.get('brew_id')
    brew = Brew.query.get(brewId)
    db.session.delete(brew)
    db.session.commit()
    return redirect('/brew')

from wtforms_alchemy import model_form_factory
from common import db
from flask_wtf import FlaskForm
from models import Brew

#http://wtforms-alchemy.readthedocs.io/en/latest/advanced.html#using-wtforms-alchemy-with-flask-wtf
BaseModelForm = model_form_factory(FlaskForm)


class ModelForm(BaseModelForm):
    @classmethod
    def get_session(self):
        return db.session


class FormFactory(object):

    _cache = {}

    def __call__(self, cls, include_keys=False):

        class Form(ModelForm):
            class Meta:
                model = cls
                include_primary_keys = include_keys

        Form.__name__ = cls.__name__ + "Form"
        self._cache.update({cls: Form})
        return Form

form_factory = FormFactory()
